/*************************************
* Lab 1 Exercise 3
* Name:
* Student No:
* Lab Group:
*************************************/

#include "node.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Copy in your implementation of the functions from ex2.
// There is one extra function called map which you have to fill up too.
// Feel free to add any new functions as you deem fit.

// Gets the tail node of a list. Returns NULL if the list is empty.
node* get_tail_node(list* lst) {
    if (!lst->head) { return NULL; }

    node* curr_node = lst->head;
    node* next_node = curr_node->next;
    while (next_node != lst->head)
    {
        curr_node = next_node;
        next_node = next_node->next;
    }
    
    return curr_node;
}

// Inserts a new node with data value at index (counting from head
// starting at 0).
// Note: index is guaranteed to be valid.
void insert_node_at(list *lst, int index, int data) {
    // Create the new node.
    node* new_node =  (node*)malloc(sizeof(node));
    new_node->data = data;

    if (lst->head == NULL)
    {
        lst->head = new_node;
        lst->head->next = new_node;
        return;
    }

    // Iterate & insert the new node.
    node* prev_node = index ? NULL : get_tail_node(lst);
    node* curr_node = lst->head;
    for (int i = 0; i < index; i++) {
        prev_node = curr_node;
        curr_node = curr_node->next;
    }
    prev_node->next = new_node;
    new_node->next = curr_node;

    // If the head has been replaced, update it.
    if (!index) { lst->head = new_node; }
}

// Deletes node at index (counting from head starting from 0).
// Note: index is guarenteed to be valid.
void delete_node_at(list *lst, int index) {
    if (!lst->head) { return; }

    // Iterate & insert the new node.
    node* prev_node = index ? NULL : get_tail_node(lst);
    node* curr_node = lst->head;
    for (int i = 0; i < index; i++) {
        prev_node = curr_node;
        curr_node = curr_node->next;
    }
    prev_node->next = curr_node->next;
    free(curr_node);

    // If the head has been replaced, update it.
    if (!index) { lst->head = prev_node->next; }
    // If the list is empty, set the head to NULL.
    if (prev_node == curr_node) { lst->head = NULL; }
}

// Rotates list by the given offset.
// Note: offset is guarenteed to be non-negative.
void rotate_list(list *lst, int offset) {
    if (!lst->head) { return; }

    while (offset--) {
        lst->head = lst->head->next;
    }
}

// Reverses the list, with the original "tail" node
// becoming the new head node.
void reverse_list(list *lst) {
    if (!lst->head) { return; }

    node* prev_node = lst->head;
    node* curr_node = prev_node->next;

    while (curr_node != lst->head)
    {
        node* next_node = curr_node->next;
        curr_node->next = prev_node;
        prev_node = curr_node;
        curr_node = next_node;
    }

    lst->head->next = prev_node;
    lst->head = prev_node;
}

// Resets list to an empty state (no nodes) and frees
// any allocated memory in the process
void reset_list(list *lst) {
    if (!lst->head) { return; }

    node* next_node = lst->head->next;
    while (next_node != lst->head)
    {
        node* deleted_node = next_node;
        next_node = next_node->next;
        free(deleted_node);
    }
    free(lst->head);
    lst->head = NULL;
}


// Traverses list and applies func on data values of
// all elements in the list.
void map(list *lst, int (*func)(int)) {
    if (!lst->head) { return; }

    node* current_node = lst->head;
    do {
        current_node->data = func(current_node->data);
        current_node = current_node->next;
    }
    while (current_node != lst->head);
}

// Traverses list and returns the sum of the data values
// of every node in the list.
long sum_list(list *lst) {
    if (!lst->head) { return 0; }

    long sum = 0;
    node* current_node = lst->head;
    do {
        sum += (long)current_node->data;
        current_node = current_node->next;
    }
    while (current_node != lst->head);
    
    printf("%ld\n", sum);
    return sum;
}