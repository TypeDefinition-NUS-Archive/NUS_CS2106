#!/bin/bash

# Source & Destination Folders
nusnetid=E0544466

# Create Temporary Folders
mkdir ./temp
mkdir ./temp/ex2
mkdir ./temp/ex3
mkdir ./temp/ex5
mkdir ./temp/ex6

# Exercise 2
cp ./ex2/node.c ./temp/ex2/node.c

# Exercise 3
cp ./ex3/node.c ./temp/ex3/node.c
cp ./ex3/function_pointers.c ./temp/ex3/function_pointers.c
cp ./ex3/ex3.c ./temp/ex3/ex3.c

# Exericse 5
cp ./ex5/check_system.sh ./temp/ex5/check_system.sh

# Exericse 6
cp ./ex6/check_syscalls.sh ./temp/ex6/check_syscalls.sh

# ZIP Folders
cd ./temp
zip -r ./${nusnetid}.zip ./*
cd ../
mv ./temp/${nusnetid}.zip ./${nusnetid}.zip

# Delete Temporary Folders
rm -r temp

# Run Checker
./ex7/check_zip.sh ${nusnetid}.zip