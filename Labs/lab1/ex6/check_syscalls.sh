#!/bin/bash

####################
# Lab 1 Exercise 5
# Name: Lim Ngian Xin Terry
# Student No: A0218430N
# Lab Group: 06
####################

echo "Printing system call report"

# Compile file
gcc -std=c99 pid_checker.c -o ex6

# Use strace to get report
strace -c ./ex6