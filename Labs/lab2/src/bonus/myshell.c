/**
 * CS2106 AY21/22 Semester 1 - Lab 2
 *
 * This file contains function definitions. Your implementation should go in
 * this file.
 */

#include "myshell.h"

// Include Standard Library
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

// Defines
#define BONUS_EX 1

#if BONUS_EX
#define MAX_SP_CMDS 4
#else
#define MAX_SP_CMDS 3
#endif

// Enums
typedef enum {
    process_status_running,
    process_status_terminating,
    process_status_stopped,
    process_status_exited,
} process_status;

// Structs
typedef struct {
    int pid, exit_status;
    process_status proc_stat;
} child_info;

typedef struct {
    char* cmd_str;
    void (*functor)(size_t, char**);
} special_command; // Special Command

// Variable(s)
child_info g_children_info[MAX_PROCESSES];
int g_num_children = 0;
special_command g_sp_cmds[MAX_SP_CMDS];

// Function Declarations
void my_wait_for_child(child_info* child, int waitpid_flags);
bool my_execute_command(size_t num_tokens, char **tokens, int* exit_status, char* err_msg);
void my_info(size_t num_tokens, char** tokens);
void my_wait(size_t num_tokens, char** tokens);
void my_terminate(size_t num_tokens, char** tokens);

#if BONUS_EX
// Variable(s)
child_info* g_waited_child = NULL;

// Function Declarations
void my_fg(size_t num_tokens, char** tokens);
void my_on_sigtstp(int signum);
void my_on_sigint(int signum);
void my_set_sigtstp_handler();
void my_set_sigint_handler();
#endif

// Assignment function(s)
void my_init(void) {
    // info
    g_sp_cmds[0].cmd_str = "info";
    g_sp_cmds[0].functor = my_info;

    // wait
    g_sp_cmds[1].cmd_str = "wait";
    g_sp_cmds[1].functor = my_wait;

    // terminate
    g_sp_cmds[2].cmd_str = "terminate";
    g_sp_cmds[2].functor = my_terminate;

#if BONUS_EX
    // fg
    g_sp_cmds[3].cmd_str = "fg";
    g_sp_cmds[3].functor = my_fg;

    // SIGTSTP & SIGINT Signal
    my_set_sigtstp_handler();
    my_set_sigint_handler();
#endif
}

void my_process_command(size_t num_tokens, char **tokens) {
    // Make a copy of the tokens array which we will modify.
    // While we can probably directly modify tokens, it is better to keep this function "pure".
    char** tokens_copy = malloc(num_tokens * sizeof(char*));
    memcpy(tokens_copy, tokens, num_tokens * sizeof(char*));

    // Split up the tokens by "&&".
    size_t cp_tokens_start = 0;
    int exit_status = 0;
    char err_msg[256] = { 0 };
    for (size_t i = 0; i < num_tokens; ++i) {
        if (!tokens_copy[i] || !strcmp(tokens_copy[i], "&&")) {
            tokens_copy[i] = NULL;

            exit_status = 0;
            memset(&err_msg[0], 0, sizeof(err_msg));
            bool success = my_execute_command(i - cp_tokens_start + 1, &tokens_copy[cp_tokens_start], &exit_status, err_msg);

            // Child process failed to start.
            if (!success) {
                printf("%s", err_msg);
                break;
            }

            // Child process started successfully but did not exit properly.
            if (exit_status) {
                printf("%s failed\n", tokens_copy[cp_tokens_start]);
                break;
            }

            cp_tokens_start = i + 1;
        }
    }

    // Delete copy of tokens.
    free(tokens_copy);
}

void my_quit(void) {
    for (int i = 0; i < g_num_children; ++i) {
        switch (g_children_info[i].proc_stat)
        {
        case process_status_running:
            killpg(g_children_info[i].pid, SIGTERM);
            g_children_info[i].proc_stat = process_status_terminating;
            break;
        case process_status_stopped:
            killpg(g_children_info[i].pid, SIGCONT);
            killpg(g_children_info[i].pid, SIGTERM);
            g_children_info[i].proc_stat = process_status_terminating;
        default:
            break;
        }
    }
    printf("Goodbye!\n");
}

// My function(s)
void my_wait_for_child(child_info* child, int waitpid_flags) {
    if (child->proc_stat == process_status_exited) {
        return;
    }

#if BONUS_EX
    g_waited_child = child;
#endif

    // The child has not yet exited. Check it's current exit_status.
    int exit_status;
    int rv = waitpid(child->pid, &exit_status, waitpid_flags);

#if BONUS_EX
    g_waited_child = NULL;
#endif
    
    // Child process finished.
    if (rv == child->pid) {
        if (WIFEXITED(exit_status) || WIFSIGNALED(exit_status)) {
            child->proc_stat = process_status_exited;
            child->exit_status = WEXITSTATUS(exit_status);
        }
        else if (WIFSTOPPED(exit_status)) {
            child->proc_stat = process_status_stopped;
        }
    }

    // Child process is still running.
    else if (rv == 0) {
        // printf("[%d] Child process is still running.\n", child->pid);
    }

    // Error occured.
    else {
        // printf("[%d] Unable to determine child process' exit status: errno(%d).\n", child->pid, errno);
    }
}

bool my_execute_command(size_t num_tokens, char **tokens, int* exit_status, char* err_msg) {
    *exit_status = 0;

    // Check for valid token size.
    if (num_tokens < 2) {
        sprintf(err_msg, "Could not execute command. Invalid number of tokens.\n");
        return false;
    }

    // Handle special commands.
    for (int i = 0; i < MAX_SP_CMDS; ++i) {
        if (!strcmp(g_sp_cmds[i].cmd_str, tokens[0])) {
            g_sp_cmds[i].functor(num_tokens, tokens);
            return true;
        }
    }

    // Check if the program exists.
    if (access(tokens[0], F_OK) == -1) {
        sprintf(err_msg, "%s not found\n", tokens[0]);
        return false;
    }

    // Check for specified I/O files.
    char* in_filename = NULL;
    char* out_filename = NULL;
    char* err_filename = NULL;
    int in_file_fd = -1;
    int out_file_fd = -1;
    int err_file_fd = -1;
    for (size_t i = 1; i < num_tokens - 1; ++i) {
        if (!tokens[i]) { continue; }
        
        if (!strcmp("<", tokens[i])) {
            in_filename = tokens[i + 1];
            tokens[i] = NULL;
        } else if (!strcmp(">", tokens[i])) {
            out_filename = tokens[i + 1];
            tokens[i] = NULL;
        } else if (!strcmp("2>", tokens[i])) {
            err_filename = tokens[i + 1];
            tokens[i] = NULL;
        }
    }

    // Open input file.
    if (in_filename) {
        in_file_fd = open(in_filename, O_RDONLY);
        if (in_file_fd == -1) {
            sprintf(err_msg, "%s does not exists\n", in_filename);
            return false;
        }
    }

    // Is the last argument is an "&"?
    int wait_for_child = (num_tokens > 2) ? strcmp(tokens[num_tokens - 2], "&") : 1;
    
    // Spawn a child process.
    int child_pid = fork();
    child_info* child = &g_children_info[g_num_children++];
    child->pid = child_pid;
    
    // Parent process.
    if (child_pid) {
        // The parent process does not need the input file.
        if (in_filename) {
            close(in_file_fd);
        }

        // Wait for child to end.
        if (wait_for_child) {
            my_wait_for_child(child, WUNTRACED);
            *exit_status = child->exit_status;
        }

        // Continue without waiting.
        else {
            printf("Child[%d] in background\n", child_pid);
        }
    }

    // Child process.
    else {
        setsid();

        // Close the standard IO file descriptors and replace them with our IO files instead.
        if (in_filename) {
            dup2(in_file_fd, STDIN_FILENO);
        }

        // Open/Create standard output file.
        if (out_filename) {
            out_file_fd = open(out_filename, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXG | S_IRWXU | S_IRWXO);
            if (out_file_fd == -1) {
                printf("%s could not be opened/created.\n", out_filename);
                exit(1);
            }
            dup2(out_file_fd, STDOUT_FILENO);
        }
        // Open/Create error output file.
        if (err_filename) {
            err_file_fd = open(err_filename, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXG | S_IRWXU | S_IRWXO);
            if (err_file_fd == -1) {
                printf("%s could not be opened/created.\n", err_filename);
                exit(1);
            }
            dup2(err_file_fd, STDERR_FILENO);
        }

        // If the last argument is an "&", remove it when passing to the program.
        // It is safe to directly modify tokens, since this is the child process, and
        // all the program memory will be replaced anyways. This does not affect the parent process.
        if (!wait_for_child) {
            tokens[num_tokens - 2] = NULL;
        }

        // Handle error.
        if (execvp(tokens[0], tokens) == -1) {
            exit(1);
        }
    }

    return true;
}

void my_info(size_t num_tokens, char** tokens) {
    // Check for valid token size.
    if (num_tokens < 2) { return; }

    for (int i = 0; i < g_num_children; ++i) {
        child_info* child = &g_children_info[i];

        // The child has exited.
        if (child->proc_stat == process_status_exited) {
            printf("[%d] Exited %d\n", child->pid, child->exit_status);
            continue;
        }

        // The child has stopped.
        else if (child->proc_stat == process_status_stopped) {
            printf("[%d] Stopped\n", child->pid);
            continue;
        }

        my_wait_for_child(child, WNOHANG);

        switch (child->proc_stat)
        {
        case process_status_stopped:
            printf("[%d] Stopped %d\n", child->pid, child->exit_status);
            break;
        case process_status_exited:
            printf("[%d] Exited %d\n", child->pid, child->exit_status);
            break;
        case process_status_running:
            printf("[%d] Running\n", child->pid);
            break;
        case process_status_terminating:
            printf("[%d] Terminating\n", child->pid);
            break;
        default:
            printf("[%d] Unhandled Process Status\n", child->pid);
            break;
        }
    }
}

void my_wait(size_t num_tokens, char** tokens) {
    // Check for valid token size.
    if (num_tokens < 3) { return; }

    // Get PID of child process.
    const int child_pid = atoi(tokens[1]);

    // Wait for child process.
    for (int i = 0; i < g_num_children; ++i) {
        if (g_children_info[i].pid == child_pid) {
            my_wait_for_child(&g_children_info[i], WUNTRACED);
            break;
        }
    }
}

void my_terminate(size_t num_tokens, char** tokens) {
    // Check for valid token size.
    if (num_tokens < 3) { return; }

    // Get PID of child process.
    const int child_pid = atoi(tokens[1]);

    // Terminate child process.
    for (int i = 0; i < g_num_children; ++i) {
        if (g_children_info[i].pid == child_pid) {
            switch (g_children_info[i].proc_stat)
            {
            case process_status_running:
                killpg(g_children_info[i].pid, SIGTERM);
                g_children_info[i].proc_stat = process_status_terminating;
                break;
            case process_status_stopped:
                killpg(g_children_info[i].pid, SIGCONT);
                killpg(g_children_info[i].pid, SIGTERM);
                g_children_info[i].proc_stat = process_status_terminating;
            default:
                break;
            }
            break;
        }
    }
}

#if BONUS_EX
void my_set_sigtstp_handler() {
    struct sigaction sigtstp_act;
    memset(&sigtstp_act, 0, sizeof(sigtstp_act));
    sigtstp_act.sa_handler = my_on_sigtstp;
    sigaction(SIGTSTP, &sigtstp_act, NULL);
}

void my_set_sigint_handler() {
    struct sigaction sigint_act;
    memset(&sigint_act, 0, sizeof(sigint_act));
    sigint_act.sa_handler = my_on_sigint;
    sigaction(SIGINT, &sigint_act, NULL);
}

void my_fg(size_t num_tokens, char** tokens) {
    // Check for valid token size.
    if (num_tokens < 3) { return; }

    // Get PID of child process.
    const int child_pid = atoi(tokens[1]);

    // Continue child process.
    for (int i = 0; i < g_num_children; ++i) {
        if (g_children_info[i].pid == child_pid) {
            if (g_children_info[i].proc_stat == process_status_stopped) {
                if (killpg(child_pid, SIGCONT) == -1) {
                    printf("[%d] could not be continued: errno(%d).\n", child_pid, errno);
                } else {
                    g_children_info[i].proc_stat = process_status_running;
                    my_wait_for_child(&g_children_info[i], WUNTRACED);
                }
            }
            break;
        }
    }
}

void my_on_sigtstp(int signum) {
    // Is there a child we are waiting for?
    if (!g_waited_child) { return; }
    
    // Stop child process.
    if (killpg(g_waited_child->pid, SIGSTOP) == -1) {
        printf("[%d] could not be stopped: errno(%d).\n", g_waited_child->pid, errno);
    } else {
        g_waited_child->proc_stat = process_status_stopped;
        printf("[%d] stopped\n", g_waited_child->pid);
    }
}

void my_on_sigint(int signum) {
    // Is there a child we are waiting for?
    if (!g_waited_child) { return; }
    
    // Interrupt child process.
    if (killpg(g_waited_child->pid, SIGINT) == -1) {
        printf("[%d] could not be interrupted: errno(%d).\n", g_waited_child->pid, errno);
    } else {
        g_waited_child->proc_stat = process_status_exited;
        printf("[%d] interrupted\n", g_waited_child->pid);
    }
}
#endif