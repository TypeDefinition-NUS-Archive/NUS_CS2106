#!/bin/bash

# Source & Destination Folders
nusnetid=E0544466
src=src

# Create Temporary Folders
mkdir ./temp
mkdir ./temp/bonus

cp ./${src}/myshell.c ./temp/myshell.c
cp ./${src}/bonus/myshell.c ./temp/bonus/myshell.c

# ZIP Folders
cd ./temp
zip -r ./${nusnetid}.zip ./*
cd ../
mv ./temp/${nusnetid}.zip ./${nusnetid}.zip

# Delete Temporary Folders
rm -r temp

# Run Checker
./${src}/check_zip.sh ${nusnetid}.zip