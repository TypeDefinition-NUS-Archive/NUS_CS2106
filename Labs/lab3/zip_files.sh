#!/bin/bash

# Source & Destination Folders
nusnetid=E0544466
src=src

# Create Temporary Folders
mkdir ./temp
mkdir ./temp/ex1
mkdir ./temp/ex2
mkdir ./temp/ex3
mkdir ./temp/ex4
mkdir ./temp/ex5
mkdir ./temp/ex6

# Copy files
cp ./${src}/ex1/packer.c ./temp/ex1/packer.c
cp ./${src}/ex2/packer.c ./temp/ex2/packer.c
cp ./${src}/ex3/packer.c ./temp/ex3/packer.c
cp ./${src}/ex4/restaurant.h ./temp/ex4/restaurant.h
cp ./${src}/ex4/restaurant.c ./temp/ex4/restaurant.c
cp ./${src}/ex5/restaurant.h ./temp/ex5/restaurant.h
cp ./${src}/ex5/restaurant.c ./temp/ex5/restaurant.c
cp ./${src}/ex6/restaurant.h ./temp/ex6/restaurant.h
cp ./${src}/ex6/restaurant.c ./temp/ex6/restaurant.c

# ZIP Folders
cd ./temp
zip -r ./${nusnetid}.zip ./*
cd ../
mv ./temp/${nusnetid}.zip ./${nusnetid}.zip

# Delete Temporary Folders
rm -r temp

# Run Checker
./${src}/check_zip.sh ${nusnetid}.zip