#include "packer.h"

// C Standard Library Includes
#include <semaphore.h>

// Barrier implementation.
// Not allowed to use pthread_barrier?
// Fine! I'll make my own barrier, with blackjack and hookers! In fact, forget the barrier!
typedef struct {
    int num_threads;
    int counter;
    sem_t entrance;
    sem_t counter_mutex;
    sem_t gate;
} mkr_barrier_t;

void mkr_barrier_init(mkr_barrier_t* barrier, int num_threads) {
    barrier->num_threads = num_threads;
    barrier->counter = 0;
    sem_init(&barrier->entrance, 0, num_threads);
    sem_init(&barrier->counter_mutex, 0, 1);
    sem_init(&barrier->gate, 0, 0);
}

void mkr_barrier_destroy(mkr_barrier_t* barrier) {
    sem_destroy(&barrier->entrance);
    sem_destroy(&barrier->counter_mutex);
    sem_destroy(&barrier->gate);
}

void mkr_barrier_wait(mkr_barrier_t* barrier) {
    // Only num_threads are allowed to enter.
    sem_wait(&barrier->entrance);

    // Increase counter and check if this is the last thread to arrive.
    sem_wait(&barrier->counter_mutex);
    if (++barrier->counter == barrier->num_threads) {
        // If this is the last thread to arrive, unlock the gate for 1 thread.
        sem_post(&barrier->gate);
    }
    sem_post(&barrier->counter_mutex);

    // Wait at the gate.
    sem_wait(&barrier->gate);

    // Decrease counter and check if this is the last thread to cross the gate.
    sem_wait(&barrier->counter_mutex);
    if (--barrier->counter) {
        // If this is NOT the last thread to cross, unlock the gate for the next thread.
        sem_post(&barrier->gate);
    } else {
        // If this is the last thread to cross, allow the next batch of num_threads to enter.
        for (int i = 0; i < barrier->num_threads; ++i) {
            sem_post(&barrier->entrance);
        }
    }
    sem_post(&barrier->counter_mutex);
}

// Global variable(s).
#define NUM_COLOURS 3
#define MAX_N 64

int g_balls_per_pack = 0;
int g_balls[NUM_COLOURS][MAX_N] = { 0 };
int g_counters[NUM_COLOURS] = { 0 };

sem_t g_mutex[NUM_COLOURS];
sem_t g_semaphores[NUM_COLOURS];
mkr_barrier_t g_barrier[NUM_COLOURS];

void packer_init(void) {
    // Write initialization code here (called once at the start of the program).
    g_balls_per_pack = 2;
    
    for (int i = 0; i < NUM_COLOURS; ++i) {
        sem_init(&g_mutex[i], 0, 1);
        sem_init(&g_semaphores[i], 0, g_balls_per_pack);
        mkr_barrier_init(&g_barrier[i], g_balls_per_pack);
    }
}

void packer_destroy(void) {
    // Write deinitialization code here (called once at the end of the program).
    for (int i = 0; i < NUM_COLOURS; ++i) {
        sem_destroy(&g_mutex[i]);
        sem_destroy(&g_semaphores[i]);
        mkr_barrier_destroy(&g_barrier[i]);
    }
}

int pack_ball(int colour, int id) {
    int other_ball = 0;

    // Change colour to start from index 0 instead of index 1.
    --colour;

    // Only allow one pack worth of threads through at a time per colour.
    sem_wait(&g_semaphores[colour]);

    // Place ball.
    sem_wait(&g_mutex[colour]);
    g_balls[colour][g_counters[colour]] = id;
    g_counters[colour] = (g_counters[colour] + 1) % g_balls_per_pack;
    sem_post(&g_mutex[colour]);

    // Populate other_ids.
    mkr_barrier_wait(&g_barrier[colour]);
    for (int i = 0; i < g_balls_per_pack; ++i) {
        if (g_balls[colour][i] != id) {
            other_ball = g_balls[colour][i];
            break;
        }
    }
    mkr_barrier_wait(&g_barrier[colour]);

    // Let the next pack through.
    sem_post(&g_semaphores[colour]);

    return other_ball;
}