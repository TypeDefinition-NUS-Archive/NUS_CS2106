#include "restaurant.h"

// C Standard Library Includes
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>

// Ticket ID.
int next_ticket_id(bool increment) {
    static int curr_ticket_id = 0;
    static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

    pthread_mutex_lock(&mutex);
    int id = curr_ticket_id;
    if (increment) { ++curr_ticket_id; }
    pthread_mutex_unlock(&mutex);
    return id;
}

// Threadsafe queue.
#define QUEUE_DATA_TYPE int

typedef struct {
    int max_size;
    int size;
    int head_index;
    int tail_index;
    QUEUE_DATA_TYPE* data;
    pthread_cond_t pop_cond;
    pthread_mutex_t mutex;
} mkr_queue_t;

void mkr_queue_init(mkr_queue_t* queue) {
    queue->max_size = 8;
    queue->size = 0;
    queue->head_index = 0;
    queue->tail_index = 0;
    queue->data = (QUEUE_DATA_TYPE*)malloc(queue->max_size * sizeof(QUEUE_DATA_TYPE));
    pthread_cond_init(&queue->pop_cond, NULL);
    pthread_mutex_init(&queue->mutex, NULL);
}

void mkr_queue_destroy(mkr_queue_t* queue) {
    free(queue->data);
    pthread_cond_destroy(&queue->pop_cond);
    pthread_mutex_destroy(&queue->mutex);
}

void mkr_queue_push(mkr_queue_t* queue, QUEUE_DATA_TYPE value) {
    pthread_mutex_lock(&queue->mutex);
    queue->data[queue->tail_index] = value; // Insert value.
    queue->tail_index = (queue->tail_index + 1) % queue->max_size;
    ++queue->size;

    // If the array is full, double it's size.
    if (queue->size == queue->max_size) {
        // Copy queue->data into a new buffer. When array is full, head_index equals tail_index.
        QUEUE_DATA_TYPE* buffer = (QUEUE_DATA_TYPE*)malloc(queue->max_size * 2 * sizeof(QUEUE_DATA_TYPE));
        int offset = queue->max_size - queue->head_index;
        memcpy(&buffer[0], &queue->data[queue->head_index], sizeof(QUEUE_DATA_TYPE) * offset);
        memcpy(&buffer[offset], &queue->data[0], sizeof(QUEUE_DATA_TYPE) * queue->head_index);

        // Update indices and max size.
        queue->head_index = 0;
        queue->tail_index = queue->max_size;
        queue->max_size *= 2;

        // Swap buffers.
        free(queue->data);
        queue->data = buffer;
    }

    pthread_cond_signal(&queue->pop_cond);
    pthread_mutex_unlock(&queue->mutex);
}

// IMPORTANT: Thread safety is NOT ensured once this function returns.
QUEUE_DATA_TYPE mkr_queue_peek(mkr_queue_t* queue) {
    pthread_mutex_lock(&queue->mutex);
    while (!queue->size) {
        pthread_cond_wait(&queue->pop_cond, &queue->mutex);
    }
    QUEUE_DATA_TYPE value = queue->data[queue->head_index];
    pthread_mutex_unlock(&queue->mutex); // Thread safety ends here.

    return value;
}

QUEUE_DATA_TYPE mkr_queue_pop(mkr_queue_t* queue) {
    pthread_mutex_lock(&queue->mutex);
    while (!queue->size) {
        pthread_cond_wait(&queue->pop_cond, &queue->mutex);
    }
    QUEUE_DATA_TYPE value = queue->data[queue->head_index];
    queue->head_index = (queue->head_index + 1) % queue->max_size;
    --queue->size;
    pthread_mutex_unlock(&queue->mutex);

    return value;
}

int mkr_queue_size(mkr_queue_t* queue) {
    pthread_mutex_lock(&queue->mutex);
    int size = queue->size;
    pthread_mutex_unlock(&queue->mutex);

    return size;
}

bool mkr_queue_is_empty(mkr_queue_t* queue) {
    pthread_mutex_lock(&queue->mutex);
    bool is_empty = (queue->size == 0);
    pthread_mutex_unlock(&queue->mutex);

    return is_empty;
}

// Threadsafe list.
#define LIST_DATA_TYPE group_state*

typedef struct mkr_list_node {
    struct mkr_list_node* next;
    pthread_mutex_t mutex;
    LIST_DATA_TYPE value;
} mkr_list_node_t;

mkr_list_node_t* mkr_list_node_create() {
    mkr_list_node_t* node = (mkr_list_node_t*)malloc(sizeof(mkr_list_node_t));
    node->next = NULL;
    pthread_mutex_init(&node->mutex, NULL);
    
    return node;
}

void mkr_list_node_destroy(mkr_list_node_t* node) {
    pthread_mutex_destroy(&node->mutex);
    free(node);
}

typedef struct {
    mkr_list_node_t* head;
} mkr_list_t;

void mkr_list_init(mkr_list_t* list) {
    list->head = mkr_list_node_create();
}

void mkr_list_destroy(mkr_list_t* list) {
    mkr_list_node_t* node = list->head;
    while (node)
    {
        mkr_list_node_t* temp = node;
        node = node->next;
        mkr_list_node_destroy(temp);
    }
}

LIST_DATA_TYPE mkr_list_peek(mkr_list_t* list, LIST_DATA_TYPE other) {
    pthread_mutex_lock(&list->head->mutex); // Lock head mutex. Head never changes.

    // Special case: List is empty.
    if (list->head->next == NULL) {
        pthread_mutex_unlock(&list->head->mutex);
        return other;
    }

    // General case: List is not empty.
    LIST_DATA_TYPE value = list->head->next->value;
    pthread_mutex_unlock(&list->head->mutex);

    return value;
}

// O(n) time insertion.
void mkr_list_push_back(mkr_list_t* list, LIST_DATA_TYPE value) {
    mkr_list_node_t* node = mkr_list_node_create();
    node->value = value;

    // Start from the head.
    mkr_list_node_t* prev = list->head;
    pthread_mutex_lock(&prev->mutex);
    
    // Special case: List is empty.
    if (prev->next == NULL) {
        // Append new node.
        prev->next = node;

        // Generate ticket ID & signal.
        node->value->ticket_id = next_ticket_id(true);
        on_enqueue();

        // Unlock mutexes.
        pthread_mutex_unlock(&prev->mutex);
        return;
    }

    // General case: List is not empty.
    mkr_list_node_t* curr = prev->next;
    pthread_mutex_lock(&curr->mutex);

    // Traverse list until the tail.
    while (curr->next != NULL) {
        pthread_mutex_unlock(&prev->mutex);
        pthread_mutex_lock(&curr->next->mutex);
        
        prev = prev->next;
        curr = curr->next;
    }

    // Append new node.
    curr->next = node;

    // Generate ticket ID & signal.
    node->value->ticket_id = next_ticket_id(true);
    on_enqueue();

    // Unlock all mutexes.
    pthread_mutex_unlock(&prev->mutex);
    pthread_mutex_unlock(&curr->mutex);
}

// Removes the first element that func() returns true on. O(n) time removal.
LIST_DATA_TYPE mkr_list_remove_first(mkr_list_t* list, bool(*func)(LIST_DATA_TYPE), LIST_DATA_TYPE other) {
    // Lock head mutex. Head never changes.
    mkr_list_node_t* prev = list->head;
    pthread_mutex_lock(&prev->mutex);

    // Special case: List is empty.
    if (prev->next == NULL) {
        pthread_mutex_unlock(&prev->mutex);
        return other;
    }

    // General case: List is not empty.
    mkr_list_node_t* curr = prev->next;
    pthread_mutex_lock(&curr->mutex);

    // Traverse list.
    bool ret_val = false;
    while (!(ret_val = func(curr->value)) && curr->next != NULL) {
        pthread_mutex_unlock(&prev->mutex);
        pthread_mutex_lock(&curr->next->mutex);
        
        prev = prev->next;
        curr = curr->next;
    }

    // Special case: No items to be removed.
    if (!ret_val) {
        pthread_mutex_unlock(&prev->mutex);
        pthread_mutex_unlock(&curr->mutex);
        return other;
    }

    // Link the previous and next node.
    prev->next = curr->next;
    pthread_mutex_unlock(&prev->mutex);

    // Delete the current node.
    LIST_DATA_TYPE value = curr->value;
    pthread_mutex_unlock(&curr->mutex);
    mkr_list_node_destroy(curr);

    return value;
}

// Restaurant function(s).
#define MAX_TABLE_SEATS 5
#define INVALID_TABLE_ID -1 

mkr_list_t g_customers;
mkr_queue_t g_tables[MAX_TABLE_SEATS];

bool assign_table(group_state* state) {
    if (!mkr_queue_is_empty(&g_tables[state->num_people-1])) {
        state->table_id = mkr_queue_pop(&g_tables[state->num_people-1]);
        return true;
    }

    return false;
}

void work_or_wait(group_state *state) {
    static pthread_cond_t wait_cond = PTHREAD_COND_INITIALIZER;
    static pthread_mutex_t wait_mutex = PTHREAD_MUTEX_INITIALIZER;
    static int worker_ticket_id = 0;
    
    while (state->table_id == INVALID_TABLE_ID) {
        // Waiting threads.
        if (state->ticket_id != worker_ticket_id) {
            pthread_mutex_lock(&wait_mutex);
            pthread_cond_wait(&wait_cond, &wait_mutex);
            pthread_mutex_unlock(&wait_mutex);
            continue;
        }
        
        // Worker thread. Constantly polling.
        group_state* next_customer = mkr_list_remove_first(&g_customers, assign_table, NULL);
        // No customer or no available tables.
        if (next_customer == NULL) {
            pthread_yield();
            continue;
        }
        
        if (next_customer->ticket_id == worker_ticket_id) {
            // This thread has gotten a table. Another thread will take over as the worker thread.
            group_state* next_worker = mkr_list_peek(&g_customers, NULL);
            worker_ticket_id = (next_worker == NULL) ? next_ticket_id(false) : next_worker->ticket_id;
        }
        pthread_cond_broadcast(&wait_cond);
    }
}

void restaurant_init(int num_tables[5]) {
    // Initialise customers.
    mkr_list_init(&g_customers);

    // Initialise tables.
    int curr_table_id = 0;
    for (int i = 0; i < MAX_TABLE_SEATS; ++i) {
        mkr_queue_init(&g_tables[i]);
        for (int j = 0; j < num_tables[i]; ++j) {
            mkr_queue_push(&g_tables[i], curr_table_id++);
        }
    }
}

void restaurant_destroy(void) {
    // Destroy customers.
    mkr_list_destroy(&g_customers);
    
    // Destroy tables.
    for (int i = 0; i < MAX_TABLE_SEATS; ++i) {
        mkr_queue_destroy(&g_tables[i]);
    }
}

// Return the id of the table you want this group to sit at.
int request_for_table(group_state *state, int num_people) {
    // Queue up.
    state->num_people = num_people;
    state->table_id = INVALID_TABLE_ID;
    mkr_list_push_back(&g_customers, state);

    // Work or wait.
    work_or_wait(state);

    return state->table_id;
}

void leave_table(group_state *state) {
    mkr_queue_push(&g_tables[state->num_people-1], state->table_id);
}