// Lim Ngian Xin Terry
// A0218430N

#include "userswap.h"

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#define PAGE_SIZE 4096

// My Struct(s)
typedef struct node_t
{
    struct node_t *next;
    void *mem_start, *mem_end;
    union
    {
        // Allocation
        struct
        {
            size_t num_pages;
        };

        // Page
        struct
        {
            int flags;
            int offset_index; // offset_index = Page index in the swapfile or backing file.
            int backing_fd;
        };
    };
} node_t;

typedef struct lorm_t
{
    size_t max_size;
    size_t size;
} lorm_t;

typedef struct swapfile_t
{
    int fd;
    int num_pages;
    int max_pages;
    node_t **pages;
} swapfile_t;

// My Function Declaration(s)
size_t byte_to_pages(size_t _size);
void sigsegv_handler(int _signo, siginfo_t *_info, void *_context);
void set_sigsegv_handler();

bool addr_in_range(void *_mem, void *_start, void *_end);
void evict_page();

void append_node(node_t *_head, node_t *_node);
node_t *remove_node(node_t *_head, void *_mem);
void push_node(node_t *_head, node_t *_node);
node_t *pop_node(node_t *_head);
node_t *get_node(node_t *_head, void *_mem);

void save_data(swapfile_t *_swapfile, node_t *_node);
void load_data(swapfile_t *_swapfile, node_t *_node);

// My Variable(s)
lorm_t g_lorm = {.max_size = 2106, .size = 0};

swapfile_t g_swapfile = {.fd = -1, .num_pages = 0, .max_pages = 0, .pages = NULL};

node_t g_pages = {.next = NULL, .mem_start = NULL, .mem_end = NULL, .flags = 0, .offset_index = -1, .backing_fd = -1};
node_t g_allocations = {.next = NULL, .mem_start = NULL, .mem_end = NULL, .num_pages = 0};
node_t g_evictions = {.next = NULL, .mem_start = NULL, .mem_end = NULL};

// Lab Function(s)
void userswap_set_size(size_t _size)
{
    g_lorm.max_size = byte_to_pages(_size);
    while (g_lorm.size > g_lorm.max_size)
    {
        evict_page();
    }
}

void *userswap_alloc(size_t _size)
{
    set_sigsegv_handler();

    size_t num_pages = byte_to_pages(_size);
    void *mem = mmap(NULL, num_pages * PAGE_SIZE, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);

    // Add to allocation list.
    node_t *a = malloc(sizeof(node_t));
    a->next = NULL;
    a->mem_start = mem;
    a->mem_end = mem + num_pages * PAGE_SIZE;
    a->num_pages = num_pages;
    append_node(&g_allocations, a);

    // Add each individual page to page list.
    for (size_t i = 0; i < num_pages; ++i)
    {
        node_t *p = malloc(sizeof(node_t));
        p->next = NULL;
        p->mem_start = mem + i * PAGE_SIZE;
        p->mem_end = mem + (i + 1) * PAGE_SIZE;
        p->flags = PROT_NONE;
        p->offset_index = -1;
        p->backing_fd = -1;
        append_node(&g_pages, p);
    }

    return mem;
}

void userswap_free(void *_mem)
{
    node_t *a = remove_node(&g_allocations, _mem);
    if (!a)
    {
        return;
    }

    for (size_t i = 0; i < a->num_pages; ++i)
    {
        void *mem = a->mem_start + i * PAGE_SIZE;
        node_t *p = remove_node(&g_pages, mem);

        if (p->flags == (PROT_READ | PROT_WRITE) && p->backing_fd >= 0)
        {
            p->flags = PROT_WRITE | PROT_READ;
            mprotect(p->mem_start, PAGE_SIZE, p->flags);
            save_data(&g_swapfile, p);
        }

        free(p);
        node_t *e = remove_node(&g_evictions, mem);
        free(e);
    }

    munmap(a->mem_start, a->num_pages * PAGE_SIZE);
    free(a);
}

void *userswap_map(int _fd, size_t _size)
{
    set_sigsegv_handler();

    size_t num_pages = byte_to_pages(_size);
    void *mem = mmap(NULL, num_pages * PAGE_SIZE, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);

    // Add to allocation list.
    node_t *a = malloc(sizeof(node_t));
    a->next = NULL;
    a->mem_start = mem;
    a->mem_end = mem + num_pages * PAGE_SIZE;
    a->num_pages = num_pages;
    append_node(&g_allocations, a);

    // Add each individual page to page list.
    for (size_t i = 0; i < num_pages; ++i)
    {
        node_t *p = malloc(sizeof(node_t));
        p->next = NULL;
        p->mem_start = mem + i * PAGE_SIZE;
        p->mem_end = mem + (i + 1) * PAGE_SIZE;
        p->flags = PROT_NONE;
        p->offset_index = i;
        p->backing_fd = _fd;
        append_node(&g_pages, p);
    }

    return mem;
}

// My Function Definition(s)
size_t byte_to_pages(size_t _size)
{
    return (_size / PAGE_SIZE) + ((_size % PAGE_SIZE) ? 1 : 0);
}

void set_sigsegv_handler()
{
    struct sigaction sigtstp_act;
    memset(&sigtstp_act, 0, sizeof(sigaction));
    sigtstp_act.sa_sigaction = sigsegv_handler;
    sigtstp_act.sa_flags = SA_RESETHAND | SA_SIGINFO;
    sigaction(SIGSEGV, &sigtstp_act, NULL);
}

void sigsegv_handler(int _signo, siginfo_t *_info, void *_context)
{
    // Get page.
    node_t *p = get_node(&g_pages, _info->si_addr);

    // Check if memory address is managed by us, or an actual segfault.
    if (!p)
    {
        return;
    }
    set_sigsegv_handler();

    // Secondary Storage -> Physical Memory
    if (p->flags == PROT_NONE)
    {
        // If this page has data in the swap file, load it.
        if (p->offset_index >= 0)
        {
            // Update flags to allow writing.
            p->flags = PROT_WRITE;
            mprotect(p->mem_start, PAGE_SIZE, p->flags);
            load_data(&g_swapfile, p);
        }

        p->flags = PROT_READ;

        // Add to eviction list.
        node_t *e = malloc(sizeof(node_t));
        e->next = NULL;
        e->mem_start = p->mem_start;
        e->mem_end = p->mem_end;
        push_node(&g_evictions, e);

        // Evict a page if the LORM is full.
        if (++g_lorm.size > g_lorm.max_size)
        {
            evict_page();
        }
    }

    // Read Only (Clean) -> Read & Write (Dirty)
    else if (p->flags == PROT_READ)
    {
        p->flags = (PROT_READ | PROT_WRITE);
    }

    // Update page flags.
    mprotect(p->mem_start, PAGE_SIZE, p->flags);
}

bool addr_in_range(void *_mem, void *_start, void *_end)
{
    return (uintptr_t)_start <= (uintptr_t)_mem && (uintptr_t)_mem < (uintptr_t)_end;
}

void evict_page()
{
    // Get first page in queue to evict.
    node_t *e = pop_node(&g_evictions);

    // Update page flags.
    node_t *p = get_node(&g_pages, e->mem_start);

    // Write to swapfile.
    if (p->flags == (PROT_READ | PROT_WRITE))
    {
        save_data(&g_swapfile, p);
    }

    p->flags = PROT_NONE;
    mprotect(p->mem_start, PAGE_SIZE, p->flags);
    madvise(p->mem_start, PAGE_SIZE, MADV_DONTNEED);

    // Update counter.
    --g_lorm.size;
    free(e);
}

void append_node(node_t *_head, node_t *_node)
{
    node_t *last = _head;
    while (last->next)
    {
        last = last->next;
    }
    last->next = _node;
}

node_t *remove_node(node_t *_head, void *_mem)
{
    node_t *prev = _head;
    node_t *curr = prev->next;
    while (curr && !addr_in_range(_mem, curr->mem_start, curr->mem_end))
    {
        prev = prev->next;
        curr = curr->next;
    }

    if (curr)
    {
        prev->next = curr->next;
    }
    return curr;
}

void push_node(node_t *_head, node_t *_node)
{
    _node->next = _head->next;
    _head->next = _node;
}

node_t *pop_node(node_t *_head)
{
    node_t *prev = _head;
    node_t *curr = prev->next;
    while (curr && curr->next)
    {
        prev = prev->next;
        curr = curr->next;
    }
    prev->next = NULL;
    return curr;
}

node_t *get_node(node_t *_head, void *_mem)
{
    node_t *prev = _head;
    node_t *curr = prev->next;
    while (curr && !addr_in_range(_mem, curr->mem_start, curr->mem_end))
    {
        prev = prev->next;
        curr = curr->next;
    }
    return curr;
}

void save_data(swapfile_t *_swapfile, node_t *_page)
{
    // Case 1: Backing file provided.
    if (_page->backing_fd >= 0)
    {
        // Set file cursor & write to file.
        lseek(_page->backing_fd, _page->offset_index * PAGE_SIZE, SEEK_SET);
        if (write(_page->backing_fd, _page->mem_start, PAGE_SIZE) != PAGE_SIZE)
        {
            printf("Error: Unable to write to backing file. errno: %d\n", errno);
            exit(1);
        }
    }

    // Case 2: No backing file provided.
    else
    {
        // Open a swapfile if not opened yet.
        if (_swapfile->fd < 0)
        {
            char filename[16] = {0};
            sprintf(filename, "%d.swap", getpid());
            _swapfile->fd = open(filename, O_RDWR | O_CREAT | O_TRUNC, S_IRWXG | S_IRWXU | S_IRWXO);
        }

        // Set file cursor & write to file.
        lseek(_swapfile->fd, _swapfile->num_pages * PAGE_SIZE, SEEK_SET);
        if (write(_swapfile->fd, _page->mem_start, PAGE_SIZE) != PAGE_SIZE)
        {
            printf("Error: Unable to write to swapfile. errno: %d\n", errno);
            exit(1);
        }
        _page->offset_index = _swapfile->num_pages;

        if (_swapfile->pages == NULL)
        {
            _swapfile->max_pages = 1024;
            _swapfile->pages = malloc(_swapfile->max_pages * sizeof(node_t *));
        }

        if (_swapfile->num_pages == _swapfile->max_pages)
        {
            node_t **buffer = malloc(2 * _swapfile->max_pages * sizeof(node_t *));
            memcpy(buffer, _swapfile->pages, _swapfile->max_pages * sizeof(node_t *));
            free(_swapfile->pages);
            _swapfile->pages = buffer;
            _swapfile->max_pages *= 2;
        }

        _swapfile->pages[_swapfile->num_pages++] = _page;
    }
}

void load_data(swapfile_t *_swapfile, node_t *_page)
{
    // Case 1: Backing file provided.
    if (_page->backing_fd >= 0)
    {
        // Set file cursor & write to file.
        lseek(_page->backing_fd, _page->offset_index * PAGE_SIZE, SEEK_SET);
        if (read(_page->backing_fd, _page->mem_start, PAGE_SIZE) != PAGE_SIZE)
        {
            printf("Error: Unable to read backing file. errno: %d\n", errno);
            exit(1);
        }
    }

    // Case 2: No backing file provided.
    else
    {
        bool shift_data = false;
        for (int i = 0; i < _swapfile->num_pages - 1; ++i)
        {
            if (_swapfile->pages[i] == _page)
            {
                _swapfile->pages[i] = _swapfile->pages[_swapfile->num_pages - 1];
                _swapfile->pages[i]->offset_index = _page->offset_index;
                shift_data = true;
                break;
            }
        }

        // Load Data into _page.
        lseek(_swapfile->fd, _page->offset_index * PAGE_SIZE, SEEK_SET);
        if (read(_swapfile->fd, _page->mem_start, PAGE_SIZE) != PAGE_SIZE)
        {
            printf("Error: Unable to read swapfile. errno: %d\n", errno);
            exit(1);
        }

        // Copy last page of data in the swapfile, and overwrite the page that is being deleted.
        if (shift_data)
        {
            unsigned char buffer[PAGE_SIZE] = {0};

            lseek(_swapfile->fd, (_swapfile->num_pages - 1) * PAGE_SIZE, SEEK_SET);
            if (read(_swapfile->fd, buffer, PAGE_SIZE) != PAGE_SIZE)
            {
                printf("Error: Unable to read swapfile. errno: %d\n", errno);
                exit(1);
            }

            lseek(_swapfile->fd, _page->offset_index * PAGE_SIZE, SEEK_SET);
            if (write(_swapfile->fd, buffer, PAGE_SIZE) != PAGE_SIZE)
            {
                printf("Error: Unable to write to swapfile. errno: %d\n", errno);
                exit(1);
            }
        }

        --_swapfile->num_pages;
        if (ftruncate(_swapfile->fd, _swapfile->num_pages * PAGE_SIZE))
        {
            printf("Error: Unable to truncate swapfile. errno: %d\n", errno);
            exit(1);
        }

        // Memory cleanup.
        if (_swapfile->num_pages == 0)
        {
            free(_swapfile->pages);
            _swapfile->pages = NULL;
            _swapfile->max_pages = 0;
        }

        // This page is no longer in our swap file.
        _page->offset_index = -1;
    }
}