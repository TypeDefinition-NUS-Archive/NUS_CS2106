#!/bin/bash

# Source & Destination Folders
nusnetid=E0544466
src=src

# Create Temporary Folders
mkdir ./temp

# Copy files
cp ./${src}/userswap.c ./temp/userswap.c
# cp ./${src}/bonus_userswap.c ./temp/bonus_userswap.c
# cp ./${src}/bonus_userswap.txt ./temp/bonus_userswap.txt

# ZIP Folders
cd ./temp
zip -r ./${nusnetid}.zip ./*
cd ../
mv ./temp/${nusnetid}.zip ./${nusnetid}.zip

# Delete Temporary Folders
rm -r temp

# Run Checker
./${src}/check_zip.sh ${nusnetid}.zip